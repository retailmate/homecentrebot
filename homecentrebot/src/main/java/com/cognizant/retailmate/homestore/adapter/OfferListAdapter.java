package com.cognizant.retailmate.homestore.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.cognizant.retailmate.homestore.R;
import com.cognizant.retailmate.homestore.model.OfferDataModel;

import java.util.ArrayList;

/**
 * Created by 452781 on 11/15/2016.
 */
public class OfferListAdapter extends RecyclerView.Adapter<OfferListAdapter.ProductHolder> {

    private ArrayList<OfferDataModel> itemsList;
    private Context mContext;

    public OfferListAdapter(Context context, ArrayList<OfferDataModel> itemsList) {
        this.itemsList = itemsList;
        this.mContext = context;
    }

    @Override
    public ProductHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.offer_single_card, null);
        ProductHolder mh = new ProductHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(ProductHolder holder, int i) {

        OfferDataModel productData = itemsList.get(i);
        holder.offerDesc.setText(productData.getOfferDescription());
        holder.validity.setText(productData.getValidity());
        holder.offerTitle.setText(productData.getOfferTitle());

    }

    @Override
    public int getItemCount() {
        return (null != itemsList ? itemsList.size() : 0);
    }

    public class ProductHolder extends RecyclerView.ViewHolder {

        protected TextView offerDesc;
        protected TextView offerTitle;
        protected TextView validity;


        public ProductHolder(View view) {
            super(view);

            this.offerDesc = (TextView) view.findViewById(R.id.offerName);
            this.offerTitle = (TextView) view.findViewById(R.id.offer_Title);
            this.validity = (TextView) view.findViewById(R.id.offer_validity);



            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    Toast.makeText(v.getContext(), offerDesc.getText(), Toast.LENGTH_SHORT).show();

                }
            });


        }

    }

}