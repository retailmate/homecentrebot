package com.cognizant.retailmate.homestore.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.cognizant.retailmate.homestore.R;
import com.cognizant.retailmate.homestore.model.modelsizechart;

import java.util.ArrayList;

/**
 * Created by 599654 on 2/17/2017.
 */


public class sizechartAdapter extends RecyclerView.Adapter<sizechartAdapter.SizechartViewHolder> {

   private ArrayList<modelsizechart> modelsizechartList;
    private Context mContext;

    public sizechartAdapter(Context context, ArrayList<modelsizechart> modelsizechartList){
        this.modelsizechartList=modelsizechartList;
        this.mContext=context;
    }
    @Override
    public SizechartViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.sizechart, viewGroup, false);
        SizechartViewHolder sizechartViewHolder=new  SizechartViewHolder(v);
        return sizechartViewHolder;
    }
    @Override
    public void onBindViewHolder(SizechartViewHolder holder, final int i) {

          modelsizechart model = modelsizechartList.get(i);
          holder.pic.setImageResource(model.getPic());
    }


    public class SizechartViewHolder extends RecyclerView.ViewHolder {

        public ImageView pic;

        public SizechartViewHolder(View view) {
            super(view);
            pic=(ImageView) view.findViewById(R.id.chartImage);
        }
    }







    @Override
    public int getItemCount() {
        return modelsizechartList.size();
    }
}
