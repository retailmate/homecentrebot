package com.cognizant.retailmate.homestore.adapter;


import android.content.Context;
import android.os.Environment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cognizant.retailmate.homestore.ChatGlobal;
import com.cognizant.retailmate.homestore.ChatUtil;
import com.cognizant.retailmate.homestore.R;
import com.cognizant.retailmate.homestore.model.ChatDataModel;
import com.cognizant.retailmate.homestore.model.OptionsAdapter;
import com.cognizant.retailmate.homestore.model.SingleProductModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;


/**Created by 543898 on 9/30/2016.**/
public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder> {
    private static final String TAG = "CustomAdapter";


    List<ChatDataModel> chatDataModelList;
    private static final String CustomTag="CustomTag";
    private Context mContext;
    ArrayList<SingleProductModel> productmodellist;
    ChatUtil util = new ChatUtil();

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(View v) {
            super(v);
        }
    }

    public class SendViewHolder extends ViewHolder {
        TextView sendMessage;
        TextView time;

        public SendViewHolder(View v) {
            super(v);
            this.sendMessage = (TextView) v.findViewById(R.id.textview_message_send);
            this.time= (TextView) v.findViewById(R.id.textview_time);
        }
    }

    public class ReceiveViewHolder extends ViewHolder {
        TextView receiveMessage;
        TextView time;

        public ReceiveViewHolder(View v) {
            super(v);
            this.receiveMessage = (TextView) v.findViewById(R.id.textview_message_receive);
            this.time= (TextView) v.findViewById(R.id.textview_time);
        }
    }

    public class ReceiveProductViewHolder extends ViewHolder {

        RecyclerView productRecyclerView;

        public ReceiveProductViewHolder(View v) {
            super(v);
            this.productRecyclerView= (RecyclerView) v.findViewById(R.id.product_list_recyc_view);
        }

    }

    public class InputViewHolder extends ViewHolder {

        RecyclerView inputRecyclerView;

        public InputViewHolder(View v) {
            super(v);
            this.inputRecyclerView = (RecyclerView) v.findViewById(R.id.input_list_recyc_view);
        }

    }

    public class InputFormViewHolder extends ViewHolder {

        RecyclerView inputFormRecyclerView;

        public InputFormViewHolder(View v) {
            super(v);
            this.inputFormRecyclerView = (RecyclerView) v.findViewById(R.id.input_list_recyc_view);
        }

    }
    public class OptionsViewHolder extends ViewHolder{
        RecyclerView optionsRecyclerView;
        public OptionsViewHolder(View v) {
            super(v);
            this.optionsRecyclerView=(RecyclerView) v.findViewById(R.id.options_list_recyc_view);
        }
    }

    public class ReceiveInvoiceViewHolder extends ViewHolder {

        TextView invoice_id, total_amount, status, purchase_date;
        RecyclerView orderholder;
        LinearLayout mainlayout;
        TextView cur_sign;
        TextView invoice_not_matched;


        public ReceiveInvoiceViewHolder(View v) {
            super(v);
            this.invoice_id = (TextView) v.findViewById(R.id.order_id_products1);
            this.total_amount = (TextView) v.findViewById(R.id.order_products_price1);
            this.status = (TextView) v.findViewById(R.id.order_products_status);
            this.purchase_date = (TextView) v.findViewById(R.id.order_products_date2);
            this.orderholder = (RecyclerView) v.findViewById(R.id.order_products_holder);
            this.mainlayout = (LinearLayout) v.findViewById(R.id.mainlayout);
            this.cur_sign = (TextView) v.findViewById(R.id.cur_sign);
            this.invoice_not_matched = (TextView) v.findViewById(R.id.invoicenotmatched);
        }

    }



    public class ShowCurrencyViewHolder extends ViewHolder {

        TextView inr, usd,aud,eur,jpy,gbp;

        public ShowCurrencyViewHolder(View v) {
            super(v);
            this.inr = (TextView) v.findViewById(R.id.inr);
            this.usd = (TextView) v.findViewById(R.id.usd);
            this.aud = (TextView) v.findViewById(R.id.aud);
            this.eur = (TextView) v.findViewById(R.id.eur);
            this.jpy = (TextView) v.findViewById(R.id.jpy);
            this.gbp = (TextView) v.findViewById(R.id.gbp);
        }
    }
    public class ShowsizechartViewHolder extends ViewHolder{

        RecyclerView sizechartRecyclerView;
        public ShowsizechartViewHolder(View v) {
            super(v);
            this.sizechartRecyclerView=(RecyclerView)v.findViewById(R.id.sizechart_recyc_view);
        }
    }


    public ChatAdapter(Context context, List<ChatDataModel> chatDataList) {

        this.chatDataModelList=chatDataList;
        this.mContext=context;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        Log.e("TAG","(o)_(o)"+viewType);
        View v;
        if (viewType == ChatGlobal.SEND) {
            v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.chat_user_send, viewGroup, false);

            return new SendViewHolder(v);
        }
        else if (viewType == ChatGlobal.RECEIVE_PRODUCT) {
            v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.product_list, viewGroup, false);
            return new ReceiveProductViewHolder(v);
        }
        else if (viewType == ChatGlobal.RECEIVE_OFFER) {
            v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.product_list, viewGroup, false);
            return new ReceiveProductViewHolder(v);
        }
        else if(viewType== ChatGlobal.SHOW_CURRENCY)
        {
            v=LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.card_search,viewGroup,false);
            return new ShowCurrencyViewHolder(v);
        }
        else if(viewType== ChatGlobal.SHOW_LCHART)
        {
            v=LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.sizechart_list,viewGroup,false);
            return new ShowsizechartViewHolder(v);
        }
        else if (viewType == ChatGlobal.SHOW_INVOICE) {
            v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.invoice, viewGroup, false);
            return new ReceiveInvoiceViewHolder(v);
        }
        else if (viewType == ChatGlobal.SHOW_INPUT) {
            v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.input_list, viewGroup, false);
            return new InputViewHolder(v);
        }else if (viewType == ChatGlobal.SHOW_FORM) {
            v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.input_list, viewGroup, false);
            return new InputFormViewHolder(v);
        }else if (viewType == ChatGlobal.RECEIVE_OPTIONS) {
            v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.options_list, viewGroup, false);
            return new OptionsViewHolder(v);
        }
        else{
            v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.chat_user_receive, viewGroup, false);
            return new ReceiveViewHolder(v);
        }

    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        if (viewHolder.getItemViewType() == ChatGlobal.SEND) {
            SendViewHolder holder = (SendViewHolder) viewHolder;
            holder.sendMessage.setText(chatDataModelList.get(position).getmDataset());
            holder.time.setText(chatDataModelList.get(position).getmTime());
        }
        else if (viewHolder.getItemViewType() == ChatGlobal.RECEIVE_PRODUCT) {
            ReceiveProductViewHolder holder = (ReceiveProductViewHolder) viewHolder;

            ProductListAdapter productDataAdapter = new ProductListAdapter(mContext, chatDataModelList.get(position).getmProductArray());

            holder.productRecyclerView.setHasFixedSize(true);
            holder.productRecyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
            holder.productRecyclerView.setAdapter(productDataAdapter);

        }
        else if (viewHolder.getItemViewType() == ChatGlobal.RECEIVE_OFFER) {
            ReceiveProductViewHolder holder = (ReceiveProductViewHolder) viewHolder;

            OfferListAdapter offerDataAdapter = new OfferListAdapter(mContext, chatDataModelList.get(position).getmOfferArray());

            holder.productRecyclerView.setHasFixedSize(true);
            holder.productRecyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
            holder.productRecyclerView.setAdapter(offerDataAdapter);

        }
        else if (viewHolder.getItemViewType() == ChatGlobal.RECEIVE_OPTIONS) {
            OptionsViewHolder  holder =(OptionsViewHolder) viewHolder;

            OptionsAdapter optionsDataAdapter = new OptionsAdapter(mContext, chatDataModelList.get(position).getOptionsList());

            holder.optionsRecyclerView.setHasFixedSize(true);
            holder.optionsRecyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
            holder.optionsRecyclerView.setAdapter(optionsDataAdapter);

        }
        else if(viewHolder.getItemViewType()==ChatGlobal.SHOW_CURRENCY)
        {
            ShowCurrencyViewHolder holder = (ShowCurrencyViewHolder) viewHolder;
            String path = Environment.getExternalStorageDirectory() + "/LandMark/Currency.xls";
            File file = new File(path);
            try {
                FileInputStream fileInputStream = new FileInputStream(file);
                Workbook wb = Workbook.getWorkbook(fileInputStream);
                Sheet sheet = wb.getSheet(0);
                holder.inr.setText(sheet.getCell(1,1).getContents().toString());
                holder.usd.setText(sheet.getCell(2,1).getContents().toString());
                holder.eur.setText(sheet.getCell(3,1).getContents());
                holder.jpy.setText(sheet.getCell(4,1).getContents());
                holder.gbp.setText(sheet.getCell(5,1).getContents());
                holder.aud.setText(sheet.getCell(6,1).getContents());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (BiffException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }   else if(viewHolder.getItemViewType()==ChatGlobal.SHOW_LCHART)
        {
            ShowsizechartViewHolder holder=(ShowsizechartViewHolder) viewHolder;
            sizechartAdapter sAdapter=new sizechartAdapter(mContext,chatDataModelList.get(position).getModelsizechartList());
            holder.sizechartRecyclerView.setHasFixedSize(true);
            holder.sizechartRecyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
            holder.sizechartRecyclerView.setAdapter(sAdapter);
        }
        else if (viewHolder.getItemViewType() == ChatGlobal.SHOW_INVOICE) {
            ReceiveInvoiceViewHolder holder = (ReceiveInvoiceViewHolder) viewHolder;

            productmodellist = new ArrayList<>();
            try {
                JSONObject orderhistory = new JSONObject(util.loadJSONFromAsset("orderhistoryoffline.json"));
                JSONArray orderarray = orderhistory.getJSONArray("Orders");
//                for (int i = 0; i < orderarray.length(); i++) {
                int sum_amount = 0;
                JSONObject finalorder = orderarray.getJSONObject(ChatGlobal.invoice_position);
//                    if (util.order_id.equals(finalorder.getString("InvoiceId"))) {
                holder.invoice_id.setText(finalorder.getString("InvoiceId"));
                holder.status.setText(finalorder.getString("OrderStatus"));
                holder.purchase_date.setText(finalorder.getString("DateCreated"));
                JSONArray finalorderarray = finalorder.getJSONArray("Items");
                for (int j = 0; j < finalorderarray.length(); j++) {
                    JSONObject finalproduct = finalorderarray.getJSONObject(j);
                    SingleProductModel singlemodel = new SingleProductModel();
                    singlemodel.setName(finalproduct.getString("ProductName"));
                    singlemodel.setType(finalproduct.getString("ProdCategory"));
                    singlemodel.setAmount(finalproduct.getInt("UnitPrice"));
                    singlemodel.setQuantity(finalproduct.getInt("Qty"));
                    String uri = finalproduct.getString("Image");
                    Log.e("TAG", "image = " + uri);
                    singlemodel.setImageId(util.getResourceId(uri, "drawable", mContext.getPackageName()));

                    // singlemodel.setImageId(finalproduct.getInt("Image"));
                    sum_amount += finalproduct.getInt("UnitPrice");
                    productmodellist.add(singlemodel);
                }
                holder.total_amount.setText(String.valueOf(sum_amount));
                holder.cur_sign.setText(OrderAdapter.sign);
//                }
                holder.mainlayout.setVisibility(View.VISIBLE);
                OrderAdapter orderadapter=new OrderAdapter(mContext,productmodellist);
                holder.orderholder.setHasFixedSize(true);
                holder.orderholder.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL,false));
                holder.orderholder.setAdapter(orderadapter);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else if (viewHolder.getItemViewType() == ChatGlobal.SHOW_INPUT) {
            InputViewHolder holder = (InputViewHolder) viewHolder;
            InputAdapter inputAdapter = new InputAdapter(mContext);
            holder.inputRecyclerView.setHasFixedSize(true);
            holder.inputRecyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
            holder.inputRecyclerView.setAdapter(inputAdapter);
        }else if (viewHolder.getItemViewType() == ChatGlobal.SHOW_FORM) {
            InputFormViewHolder holder = (InputFormViewHolder) viewHolder;
            InputFormAdapter inputFormAdapter=new InputFormAdapter(mContext);
            holder.inputFormRecyclerView.setHasFixedSize(true);
            holder.inputFormRecyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
            holder.inputFormRecyclerView.setAdapter(inputFormAdapter);
        }
        else  {
            ReceiveViewHolder holder = (ReceiveViewHolder) viewHolder;
            holder.receiveMessage.setText(chatDataModelList.get(position).getmDataset());
            holder.time.setText(chatDataModelList.get(position).getmTime());
        }
    }

    @Override
    public int getItemCount() {
        Log.e("TAG","(o)_(o)"+chatDataModelList.size());
        return chatDataModelList.size();
    }

    @Override
    public int getItemViewType(int position) {
//
        return chatDataModelList.get(position).getmDatasetTypes();
    }


}
