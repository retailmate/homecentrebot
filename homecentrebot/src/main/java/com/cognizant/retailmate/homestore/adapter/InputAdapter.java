package com.cognizant.retailmate.homestore.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.cognizant.retailmate.homestore.ChatUtil;
import com.cognizant.retailmate.homestore.R;

/**
 * Created by 540472 on 3/23/2017.
 */
public class InputAdapter extends RecyclerView.Adapter<InputAdapter.InputViewHolder> {

    Context context;

    public InputAdapter(Context context) {
        this.context = context;
    }


    @Override
    public InputViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.input_invoice,parent,false);
        InputViewHolder inputViewHolder=new InputViewHolder(view);
        return inputViewHolder;
    }

    @Override
    public void onBindViewHolder(InputViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 1;
    }

    public class InputViewHolder extends RecyclerView.ViewHolder {
        EditText input_invoice;
        Button go_btn;
        public InputViewHolder(View itemView) {
            super(itemView);

            input_invoice=(EditText)itemView.findViewById(R.id.invoice_no);
            go_btn=(Button)itemView.findViewById(R.id.start_show);
            go_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.e("###","Button clicked");
                    ChatUtil util=new ChatUtil();
                    util.check_invoice_existance(input_invoice.getText().toString());
                    util.order_id=input_invoice.getText().toString();
                }
            });
        }
    }
}
