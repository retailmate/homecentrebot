package com.cognizant.retailmate.homestore.model;

/**
 * Created by 599584 on 3/21/2017.
 */

public class SingleInvoice {
    String heading;

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }
}
