package com.cognizant.retailmate.homestore.model;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cognizant.retailmate.homestore.R;

import java.util.ArrayList;

/**
 * Created by 540472 on 12/22/2016.
 */

/**
 * Created by 599584 on 3/27/2017.
 */

public class OptionsAdapter extends RecyclerView.Adapter<OptionsAdapter.ProductHolder> {
        private static final String CustomTag = "CustomTag";
    public static String genre;

        private ArrayList<OptionsDataModel>  OptionsList;
        private Context mContext;

        public OptionsAdapter(Context context, ArrayList<OptionsDataModel> OptionsList) {
            this.OptionsList = OptionsList;
            this.mContext = context;
            Log.e(CustomTag,"#### SuggestionListAdapter");
        }

        @Override
        public ProductHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.optionbutton, null);
            ProductHolder mh = new ProductHolder(v);
            return mh;
        }

        @Override
        public void onBindViewHolder(ProductHolder holder, final int i) {
            System.out.println("#### onBindViewHolder" );
            final OptionsDataModel optionsDataModel = OptionsList.get(i);
            System.out.println("#### suggestData.getSuggestion()" + optionsDataModel.getOption());
            holder.optionText.setText(optionsDataModel.getOption());
            holder.suggestLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    LocalBroadcastManager localBroadcastManager = LocalBroadcastManager
                            .getInstance(mContext);
                    Intent intent = new Intent("option.click");
                    intent.putExtra("reply",optionsDataModel.getOption());
                    mContext.sendBroadcast(intent);
                    localBroadcastManager.sendBroadcast(intent);

//                    Log.e(CustomTag,"onClick viewHolder");
//                    if(optionsDataModel.getOption().equals("Ladies")){
//                        genre="ladies";
//
//                    }
//                    else if(optionsDataModel.getOption().equals("gents"))
//                    {
//                        genre="gents";
//
//                    }
//                    else if(optionsDataModel.getOption().equals("kids"))
//                    {
//                        genre="kids";
//                    }







                }
            });
        }

        @Override
        public int getItemCount() {
            return OptionsList.size();
        }

        public class ProductHolder extends RecyclerView.ViewHolder {

            protected TextView optionText;
            protected LinearLayout suggestLayout;


            public ProductHolder(final View view) {
                super(view);

                this.optionText = (TextView) view.findViewById(R.id.option);
                this.suggestLayout = (LinearLayout) view.findViewById(R.id.option_linear_layout);


            }

        }

    }

