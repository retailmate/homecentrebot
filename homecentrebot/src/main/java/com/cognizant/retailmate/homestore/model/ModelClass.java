package com.cognizant.retailmate.homestore.model;

import java.util.List;

public class ModelClass {


    private double confidence;
    private String type;
    private String action;
    private String msg;

    private List<String> quickreplies;
    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    private EntitiesBean entities;

    public double getConfidence() {
        return confidence;
    }

    public void setConfidence(double confidence) {
        this.confidence = confidence;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public EntitiesBean getEntities() {
        return entities;
    }
    public void setQuickreplies(List<String> quickreplies){
        this.quickreplies = quickreplies;
    }
    public List<String> getQuickreplies(){
        return this.quickreplies;
    }

    public void setEntities(EntitiesBean entities) {
        this.entities = entities;
    }

    public static class EntitiesBean {
        private List<PersonBean> person;
        private List<IntentBean> intent;

        public List<PersonBean> getPerson() {
            return person;
        }

        public void setPerson(List<PersonBean> person) {
            this.person = person;
        }

        public List<IntentBean> getIntent() {
            return intent;
        }

        public void setIntent(List<IntentBean> intent) {
            this.intent = intent;
        }

        public static class PersonBean {
            /**
             * confidence : 0.8325042685323366
             * type : value
             * value : soho
             * suggested : true
             */

            private double confidence;
            private String type;
            private String value;
            private boolean suggested;

            public double getConfidence() {
                return confidence;
            }

            public void setConfidence(double confidence) {
                this.confidence = confidence;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public String getValue() {
                return value;
            }

            public void setValue(String value) {
                this.value = value;
            }

            public boolean isSuggested() {
                return suggested;
            }

            public void setSuggested(boolean suggested) {
                this.suggested = suggested;
            }
        }

        public static class IntentBean {
            /**
             * confidence : 0.9882574840960712
             * value : greetings
             */

            private double confidence;
            private String value;

            public double getConfidence() {
                return confidence;
            }

            public void setConfidence(double confidence) {
                this.confidence = confidence;
            }

            public String getValue() {
                return value;
            }

            public void setValue(String value) {
                this.value = value;
            }
        }
    }
}
