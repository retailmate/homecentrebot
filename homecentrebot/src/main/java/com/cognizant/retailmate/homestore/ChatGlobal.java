package com.cognizant.retailmate.homestore;

import android.content.Context;
import android.speech.tts.TextToSpeech;
import android.support.v7.widget.RecyclerView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cognizant.retailmate.homestore.adapter.ChatAdapter;
import com.cognizant.retailmate.homestore.model.ChatDataModel;
import com.cognizant.retailmate.homestore.model.OptionsDataModel;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * Created by Guest_User on 22/11/16.
 */

public class ChatGlobal {
    public static HashSet<Character> arabicSet= new HashSet<>();
    public static boolean arabicSpeaking=false;
    public static boolean isEmail=false;
    public static boolean isDelivery=false;
    public static boolean isDeliveryYes=false;
    public static boolean isDeliveryNo=false;
    public static boolean isInvoice=false;
    public static boolean isInvoiceYes=false;
    public static boolean isInvoiceNo=false;
    public  static boolean isEmailYes=false;
    public static boolean isEmailNo=false;
    /**
     *  Refactored searchAPIToken to googleToken
     * */

    public ChatGlobal(Context context) {
        this.mContext= context;
    }

    public static final int SEND = 0;
    public static final int RECEIVE = 1;
    public static final int RECEIVE_PRODUCT = 2;
    public static final int RECEIVE_OFFER = 3;
    public static final int SHOW_CURRENCY = 4;
    public static final int SHOW_INVOICE = 5;
    public static final int SHOW_INPUT = 6;
    public static final int SHOW_LCHART= 7;
    public static final int RECEIVE_OPTIONS= 8;
    public static final int SHOW_FORM= 9;
    // Base url for Luis
    public static String baseURL="https://westus.api.cognitive.microsoft.com/luis/v2.0/apps/3af205c0-d01e-4b20-a67d-3ebdd7140791?subscription-key=665d662ecb614e40a54cf8d166989398&q=";
    //token for search api
    public static String googleToken ="eyJhbGciOiJSUzI1NiIsImtpZCI6IjkyNmQ2MGNkMDU1OTExZjc1ZjMyNGFkZGUxNzA4ZDkzMmY5NTk1YWYifQ.eyJpc3MiOiJodHRwczovL2FjY291bnRzLmdvb2dsZS5jb20iLCJpYXQiOjE0ODc2NzQxMzMsImV4cCI6MTQ4NzY3NzczMywiYXVkIjoiMjE2NzU3ODY0NTQ2LWI2Z21hbWI2c2FrNmtqb2o5amVzZWpibXNvaW43NTQ1LmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwic3ViIjoiMTE3NTYzNjc1MDA4NDE4MDA2NTA3IiwiZW1haWxfdmVyaWZpZWQiOnRydWUsImF6cCI6IjIxNjc1Nzg2NDU0Ni1wYzljMG51cmNxaG81M2loNjJrMDhpOTMyOTZjbDg2MS5hcHBzLmdvb2dsZXVzZXJjb250ZW50LmNvbSIsImVtYWlsIjoicmV0YWlsbWF0ZWF4QGdtYWlsLmNvbSIsIm5hbWUiOiJBbWVyIEhhc2FuIiwicGljdHVyZSI6Imh0dHBzOi8vbGgzLmdvb2dsZXVzZXJjb250ZW50LmNvbS8tMjkwbUZGX1lIaUkvQUFBQUFBQUFBQUkvQUFBQUFBQUFBQUEvQURQbGhmS0VMd1ZZNzluWWxzdWo0M2JtYW02eUxibjVHdy9zOTYtYy9waG90by5qcGciLCJnaXZlbl9uYW1lIjoiQW1lciIsImZhbWlseV9uYW1lIjoiSGFzYW4iLCJsb2NhbGUiOiJlbiJ9.aPi2mEnlwyISRmXcuOQx2JbnN4SLvuhJURnH75eAdxbq0NTPGmr07fL9Fht3WKQYP62Z6VK9XbBSkKsq8Q0hvxXfKkaJD3POT42K__E2PLnkYd7B_jqmhZcdurKDlMliriuQiqXbEvLuAWV1YP9S9f8oDiEj5cT10ovmnAcnIfkAeYTOpK6cocnzQ9MpUZAkYO-v-_yEf6QzOZSg0CdxcUOFDqUoFLDAzVASNQXEC0t9h49PjCVhyqk4dyQPv6XPnfShwk3BqJzeP32CkrC_4vo6DyI6_7YWk3xu2p8kndims7_IP0bfVaOfL0zLqTDO7hZd_AZSHRxs_BQKJBg_Cw";
//
    //for YES/NO(means optional) response
    public static boolean isAsked=false;
    public static boolean askedSpecific=false;
    public static boolean intentIsRecommend=false;
    public static boolean intentIsAvailable=false;
    public static boolean intentIsPrice=false;
    public static boolean intentIsAddToCart=false;
    public static boolean isaddToCartAPI=false;
    //for mandatory response
    public static boolean isPrompt=false;
    public static String imageBaseUrl="https://Nikedevret.cloudax.dynamics.com/mediaserver/";
    public static String recommendAPI="http://rmnikeapiapp.azurewebsites.net/api/CognitiveServices/GetUserToItemRecommendationsAPI";
    public static String baseAddToCartAPI="http://nikecustomervalidationax.azurewebsites.net/api/cart/CreateCart?idToken=";
    public static String baseProductSearchAPI="https://Nikedevret.cloudax.dynamics.com/Commerce/Products/SearchByText(channelId=68719476778,catalogId=0,searchText=%27";
    public static boolean tapToAdd=false;
    public static StringBuffer cartBuffer=new StringBuffer();
    public static String productID="";
    public static String userId="004021";
    public static String intent="";

    public static TextToSpeech textToSpeech;
    public static int result;
    public static Context mContext;
    public static boolean online=true;

    //FOR GOOGLE SEARCH IN DEFAULT CASE.
    //https://www.googleapis.com/customsearch/v1?q=do+you+have+dell+laptop&cx=006896418020272592208%3Agznxrju2_ha&filter=1&num=5&safe=medium&key=AIzaSyAPXvMXt1MR5PMYMH7hoi2Pvfx2deHz6TQ
    public static String baseSearchUrl="https://www.googleapis.com/customsearch/v1?q=";
    public static String endSearchUrl="&cx=006896418020272592208%3Agznxrju2_ha&filter=1&num=5&safe=medium&key=AIzaSyAPXvMXt1MR5PMYMH7hoi2Pvfx2deHz6TQ";
    public static boolean hasSearchImage=false;

    //Intent specific Info
    public static String contextId;
    public static String lastIntent="";

    //Layout Components
    public static RecyclerView mRecyclerView;
    public static ChatAdapter mAdapter;
    public static EditText chatText;
    public static String genre;
    public static ImageButton sendButton;
    public static RecyclerView.LayoutManager mLayoutManager;

    public static List<String> mDataset;
    public static ArrayList<Integer> mDatasetTypes;

    public static List<String> entityItemList =new ArrayList<>();
    public static List<String> entityBooleanList =new ArrayList<>();
    public static List<Integer> entityNumberList =new ArrayList<>();
    public static List<String> itemMatchedList =new ArrayList<>();
    public static List<String> itemMatchedIdList = new ArrayList<>();
    public static List<OptionsDataModel> optionsDataModel;
    public static List<ChatDataModel> chatDataModels;

    public static boolean variantNeeded=true;
    public static boolean isVariantAPI=false;
    //for translation in arabic
    public static String eng2arabString="https://translate.yandex.net/api/v1.5/tr.json/translate?lang=en-ar&key=trnsl.1.1.20170314T112944Z.0d019066b884a9c7.be1a27e78ecfc1319458aae64794fac4d10cdd3f&text=";
    public static String arab2engString="https://translate.yandex.net/api/v1.5/tr.json/translate?lang=ar-en&key=trnsl.1.1.20170314T112944Z.0d019066b884a9c7.be1a27e78ecfc1319458aae64794fac4d10cdd3f&text=";

    public static String converted="";

    public static int countPrompt=0;
    public static String mSender="Amer Hasan";
    public static String mRecipient = "Raymont Brattly";

    public static Boolean invoicematched=false;
    public static TextView invoice_id,total_amount,status,purchase_date;
    public static RecyclerView invoiceHolder;
    public static LinearLayout invoiceLinearLayout;
    public static TextView cur_sign;
    public static TextView invoice_not_matched;
    public static int invoice_position;
    public static String input_mobile_no="";
    public static String input_email="";
    public static String text="";



}
