package com.cognizant.retailmate.homestore.model;

/**
 * Created by 540472 on 3/21/2017.
 */
public class CurrencyModel {
    Double currencyvalue;

    public Double getCurrencyvalue() {
        return currencyvalue;
    }

    public void setCurrencyvalue(Double currencyvalue) {
        this.currencyvalue = currencyvalue;
    }
}
