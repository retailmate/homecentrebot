package com.cognizant.retailmate.homestore.adapter;

/**
 * Created by 599584 on 3/21/2017.
 */


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cognizant.retailmate.homestore.R;
import com.cognizant.retailmate.homestore.model.SingleProductModel;

import java.util.ArrayList;

/**
 * Created by Guest_User on 21/03/17.
 */

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.ProductViewHolder> {

    Context context;
    ArrayList<SingleProductModel> singleProductModelArrayList;
    public static String sign="AED";
    Integer ImageId;

    public OrderAdapter(Context context, ArrayList<SingleProductModel> singleProductModelArrayList) {
        this.context = context;
        this.singleProductModelArrayList = singleProductModelArrayList;
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.order_product_item,parent,false);
        ProductViewHolder productViewHolder=new ProductViewHolder(view);
        return productViewHolder;
    }

    @Override
    public void onBindViewHolder(ProductViewHolder holder, int position) {
        SingleProductModel singleProductModel=singleProductModelArrayList.get(position);
        ImageId=singleProductModel.getImageId();
        holder.ProductImage.setImageResource(ImageId);
        holder.ProductName.setText(singleProductModel.getName());
        holder.ProductCategory.setText(singleProductModel.getType());
        holder.Quantity.setText(String.valueOf(singleProductModel.getQuantity()));
        holder.CurrencySign.setText(sign);
        holder.Amount.setText(String.valueOf(singleProductModel.getAmount()));

    }

    @Override
    public int getItemCount() {
        return singleProductModelArrayList.size();
    }

    public class ProductViewHolder extends RecyclerView.ViewHolder{

        TextView ProductName,ProductCategory,Quantity,Amount,CurrencySign;
        ImageView ProductImage;

        public ProductViewHolder(View itemView) {
            super(itemView);
            ProductName=(TextView) itemView.findViewById(R.id.order_product_prodname1);
            ProductCategory=(TextView) itemView.findViewById(R.id.order_product_prodcategory);
            Quantity=(TextView) itemView.findViewById(R.id.product_quantity_main1);
            Amount=(TextView) itemView.findViewById(R.id.order_product_prodprice1);
            CurrencySign=(TextView) itemView.findViewById(R.id.sign);
            ProductImage=(ImageView) itemView.findViewById(R.id.order_products_flag);
        }
    }
}


