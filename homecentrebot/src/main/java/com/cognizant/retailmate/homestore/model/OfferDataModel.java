package com.cognizant.retailmate.homestore.model;

/**
 * Created by 452781 on 11/15/2016.
 */
public class OfferDataModel {
    String offerTitle;
    String offerDescription;
    String validity;

    public String getOfferTitle() {
        return offerTitle;
    }

    public void setOfferTitle(String offerTitle) {
        this.offerTitle = offerTitle;
    }

    public String getValidity() {
        return validity;
    }

    public void setValidity(String validity) {
        this.validity = validity;
    }


    public String getOfferDescription() {
        return offerDescription;
    }

    public void setOfferDescription(String offerDescription) {
        this.offerDescription = offerDescription;
    }
}