package com.cognizant.retailmate.homestore;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Build;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.ActivityCompat;
import android.text.format.DateFormat;
import android.util.Log;
import android.widget.Toast;

import com.cognizant.retailmate.homestore.email.BackgroundMail;
import com.cognizant.retailmate.homestore.model.ChatDataModel;
import com.cognizant.retailmate.homestore.model.OfferDataModel;
import com.cognizant.retailmate.homestore.model.OptionsDataModel;
import com.cognizant.retailmate.homestore.model.ProductDataModel;
import com.cognizant.retailmate.homestore.model.modelsizechart;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import static com.cognizant.retailmate.homestore.MainActivity.context;
import static com.cognizant.retailmate.homestore.MainActivity.startspeaking;

/**
 * Created by 540472 on 12/27/2016.
 */
public class ChatUtil {

    private static final String CustomTag="CustomTag";
    public String order_id;
    String emailid="retailmateuser31@gmail.com";
    String password="testtest100";
    String recipient="iotretailmate@gmail.com";
    static int result = 5432;
    public static TextToSpeech textToSpeech;
    // Storage Permissions
    public static final int REQUEST_EXTERNAL_STORAGE = 1;
    public static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };


    public String loadJSONFromAsset(String fileName) {
        String json = null;
        try {
            InputStream is = ChatGlobal.mContext.getAssets().open(fileName);

            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");

        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }


    @SuppressWarnings("deprecation")
    private static void ttsUnder20(String text) {
        HashMap<String, String> map = new HashMap<>();
        map.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "MessageId");
        ChatGlobal.textToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, map);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private static void ttsGreater21(String text) {
        String utteranceId=ChatGlobal.mContext.hashCode() + "";
        ChatGlobal.textToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, null, utteranceId);
    }



    //Used for getting resource id of offer images
    public int getResourceId(String pVariableName, String pResourcename, String pPackageName)
    {
        try {
            return ChatGlobal.mContext.getResources().getIdentifier(pVariableName, pResourcename, pPackageName);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    public void setSendMessage(String userSays){
        ChatDataModel chatSendData=new ChatDataModel();
        chatSendData.setmDataset(userSays);
        chatSendData.setmDatasetTypes(ChatGlobal.SEND);
        Date d = new Date();
        String time = (String) DateFormat.format("HH:mm", d.getTime());
        chatSendData.setmTime(time);
        ChatGlobal.chatDataModels.add(chatSendData);
        ChatGlobal.mAdapter.notifyDataSetChanged();
        ChatGlobal.mRecyclerView.scrollToPosition(ChatGlobal.chatDataModels.size()-1);
    }
    public void setSuggestDataModels(){
        OptionsDataModel optionsDataModel = new OptionsDataModel();
        optionsDataModel.setOption("Ladies");
        ChatGlobal.optionsDataModel.add(optionsDataModel);

        OptionsDataModel optionsDataModel1 = new OptionsDataModel();
        optionsDataModel1.setOption("Gents");
        ChatGlobal.optionsDataModel.add(optionsDataModel1);

        OptionsDataModel optionsDataModel2 = new OptionsDataModel();
        optionsDataModel2.setOption("Kids");
        ChatGlobal.optionsDataModel.add(optionsDataModel2);


    }

    public void setSuggestBooleanModels(){
        OptionsDataModel optionsDataModel = new OptionsDataModel();
        optionsDataModel.setOption("Yes");
        ChatGlobal.optionsDataModel.add(optionsDataModel);

        OptionsDataModel optionsDataModel1 = new OptionsDataModel();
        optionsDataModel1.setOption("No");
        ChatGlobal.optionsDataModel.add(optionsDataModel1);

    }
    public void setOption(List<OptionsDataModel> OptionsList) {
        Log.e("TAG", "#### setSuggestion");
        ChatDataModel chatSuggestData = new ChatDataModel();
        ArrayList<OptionsDataModel> optionsDataModelArrayList = new ArrayList<>();
        chatSuggestData.setmDatasetTypes(ChatGlobal.RECEIVE_OPTIONS);
        for (int i = 0; i < OptionsList.size(); i++) {
            OptionsDataModel optionsDataModel = new OptionsDataModel();
            optionsDataModel.setOption(OptionsList.get(i).getOption());
            optionsDataModelArrayList.add(optionsDataModel);
        }
        System.out.println("#### suggestDataModelArrayList" + optionsDataModelArrayList.get(0).getOption());
        chatSuggestData.setOptionsList(optionsDataModelArrayList);
        ChatGlobal.chatDataModels.add(chatSuggestData);
        ChatGlobal.mAdapter.notifyDataSetChanged();
        ChatGlobal.mRecyclerView.scrollToPosition(ChatGlobal.chatDataModels.size() - 1);
    }

    public void setReplyProduct(List<ProductDataModel> productList){
        ChatDataModel chatReceiveData=new ChatDataModel();
        ArrayList<ProductDataModel> productDataModelArrayList=new ArrayList<>();
        chatReceiveData.setmDatasetTypes(ChatGlobal.RECEIVE_PRODUCT);
        Date d1 = new Date();
        String time1 = (String) DateFormat.format("HH:mm", d1.getTime());
        chatReceiveData.setmTime(time1);
        for (int i = 0; i <productList.size() ; i++) {
            ProductDataModel productDataModel=new ProductDataModel();
            productDataModel.setProductName(productList.get(i).getProductName());
            productDataModel.setPrice(productList.get(i).getPrice());
            productDataModel.setProductImageResource(productList.get(i).getProductImageResource());
            productDataModel.setProductId(productList.get(i).getProductId());
            productDataModelArrayList.add(productDataModel);
        }
        chatReceiveData.setmProductArray(productDataModelArrayList);
        ChatGlobal.chatDataModels.add(chatReceiveData);
        ChatGlobal.mAdapter.notifyDataSetChanged();
        ChatGlobal.mRecyclerView.scrollToPosition(ChatGlobal.chatDataModels.size()-1);
    }


    public void setReplyOffer(ArrayList<OfferDataModel> offerDataModelArrayList, ChatDataModel chatOfferData){
        Log.e("###","replyoffer ----"+offerDataModelArrayList.get(0).getOfferDescription());
        chatOfferData.setmOfferArray(offerDataModelArrayList);
        ChatGlobal.chatDataModels.add(chatOfferData);
        ChatGlobal.mAdapter.notifyDataSetChanged();
        ChatGlobal.mRecyclerView.scrollToPosition(ChatGlobal.chatDataModels.size()-1);
    }

    public void setReplyProductOffline(ArrayList<ProductDataModel> productDataModelArrayList,ChatDataModel chatReceiveData ){

        chatReceiveData.setmProductArray(productDataModelArrayList);
        ChatGlobal.chatDataModels.add(chatReceiveData);
        ChatGlobal.mAdapter.notifyDataSetChanged();
        ChatGlobal.mRecyclerView.scrollToPosition(ChatGlobal.chatDataModels.size()-1);
    }
    public void setsizechart(ArrayList<modelsizechart> modelsizechartArrayList, ChatDataModel chatReceiveData ){

        chatReceiveData.setModelsizechartList(modelsizechartArrayList);
        ChatGlobal.chatDataModels.add(chatReceiveData);
        ChatGlobal.mAdapter.notifyDataSetChanged();
        ChatGlobal.mRecyclerView.scrollToPosition(ChatGlobal.chatDataModels.size()-1);
    }

    public static void setReplyMessage(String reply){


        ChatDataModel chatSendData=new ChatDataModel();
        chatSendData.setmDataset(reply);
        chatSendData.setmDatasetTypes(ChatGlobal.RECEIVE);
        Date d = new Date();
        String time = (String) DateFormat.format("HH:mm", d.getTime());
        chatSendData.setmTime(time);
        ChatGlobal.chatDataModels.add(chatSendData);
        ChatGlobal.mAdapter.notifyDataSetChanged();
        ChatGlobal.mRecyclerView.scrollToPosition(ChatGlobal.chatDataModels.size()-1);
        startspeaking(reply);


    }
    public void check_invoice_existance(String inv) {
//        ChatGlobal.invoiceModelList =new ArrayList<>();
        try
        {
            JSONObject orderhistory=new JSONObject(loadJSONFromAsset("orderhistoryoffline.json"));
            JSONArray orderarray=orderhistory.getJSONArray("Orders");
            for(int i=0;i<orderarray.length();i++)
            {
                int sum_amount=0;
                JSONObject finalorder=orderarray.getJSONObject(i);
                if(inv.equals(finalorder.getString("InvoiceId")))
                {
                    Log.e("###","Invoice found");
                    ChatGlobal.invoice_position=i;
                    ChatGlobal.invoicematched=true;
                }
            }
            if(ChatGlobal.invoicematched==true){
                setInvoice();
                setReplyMessage(context.getResources().getString(R.string.otherinvoice));
                loadOptionChart();
//                showInput();
                ChatGlobal.isInvoiceNo=false;
                ChatGlobal.isInvoiceYes=false;
                ChatGlobal.invoicematched=false;
            }else {
                setReplyMessage(context.getResources().getString(R.string.noorder));
            }
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    public void loadOptionChart()
    {
        ChatGlobal.optionsDataModel = new ArrayList<>();
        setSuggestBooleanModels();
        setOption(ChatGlobal.optionsDataModel);
    }
    private void setInvoice() {
        ChatGlobal.invoicematched=false;
        ChatDataModel chatRecommendData = new ChatDataModel();
        chatRecommendData.setmDatasetTypes(ChatGlobal.SHOW_INVOICE);
        Date d1 = new Date();
        String time1 = (String) DateFormat.format("HH:mm", d1.getTime());
        chatRecommendData.setmTime(time1);
        ChatGlobal.chatDataModels.add(chatRecommendData);
        ChatGlobal.mAdapter.notifyDataSetChanged();
        ChatGlobal.mRecyclerView.scrollToPosition(ChatGlobal.chatDataModels.size()-1);
    }

    public static void showInput()
    {

        ChatDataModel chatRecommendData = new ChatDataModel();

        chatRecommendData.setmDatasetTypes(ChatGlobal.SHOW_INPUT);
        Date d1 = new Date();
        String time1 = (String) DateFormat.format("HH:mm", d1.getTime());
        chatRecommendData.setmTime(time1);
        ChatGlobal.chatDataModels.add(chatRecommendData);
        ChatGlobal.mAdapter.notifyDataSetChanged();
        ChatGlobal.mRecyclerView.scrollToPosition(ChatGlobal.chatDataModels.size()-1);
    }

//    public static void startspeaking(String to_tell)
//    {
//        if (result == textToSpeech.LANG_NOT_SUPPORTED || result == textToSpeech.LANG_MISSING_DATA)
//            Toast.makeText(context, "Not supported in your device", Toast.LENGTH_SHORT).show();
//        else {
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                ttsGreater21(to_tell);
//            } else {
//                ttsUnder20(to_tell);
//            }
//        }
//    }

    public void sendmail(String subject,String body)
    {
        BackgroundMail.newBuilder(ChatGlobal.mContext)
                .withUsername(emailid)
                .withPassword(password)
                .withMailto(recipient)
                .withType(BackgroundMail.TYPE_PLAIN)
                .withSubject(subject)
                .withBody(body)
                .withProcessVisibility(false)
                .withOnSuccessCallback(new BackgroundMail.OnSuccessCallback() {
                    @Override
                    public void onSuccess() {
                        //do some magic
                    }
                })
                .withOnFailCallback(new BackgroundMail.OnFailCallback() {
                    @Override
                    public void onFail() {
                        //do some magic
                    }
                })
                .send();
    }

    public static void showInputForm()
    {

        ChatDataModel chatRecommendData = new ChatDataModel();

        chatRecommendData.setmDatasetTypes(ChatGlobal.SHOW_FORM);
        Date d1 = new Date();
        String time1 = (String) DateFormat.format("HH:mm", d1.getTime());
        chatRecommendData.setmTime(time1);
        ChatGlobal.chatDataModels.add(chatRecommendData);
        ChatGlobal.mAdapter.notifyDataSetChanged();
        ChatGlobal.mRecyclerView.scrollToPosition(ChatGlobal.chatDataModels.size()-1);
    }

    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }


}
