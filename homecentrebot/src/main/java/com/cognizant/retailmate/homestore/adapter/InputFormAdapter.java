package com.cognizant.retailmate.homestore.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.cognizant.retailmate.homestore.ChatGlobal;
import com.cognizant.retailmate.homestore.ChatUtil;
import com.cognizant.retailmate.homestore.R;

/**
 * Created by 540472 on 3/29/2017.
 */
public class InputFormAdapter extends RecyclerView.Adapter<InputFormAdapter.InputFormViewHolder>{
    Context context;

    public InputFormAdapter(Context context) {
        this.context = context;
    }

    @Override
    public InputFormViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.chat_form_input,parent,false);
        InputFormViewHolder inputFormViewHolder=new InputFormViewHolder(view);
        return inputFormViewHolder;
    }

    @Override
    public void onBindViewHolder(InputFormViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 1;
    }

    public class InputFormViewHolder extends RecyclerView.ViewHolder{
        EditText mob_no;
        EditText email;
        Button go_btn;
        boolean mob_ok=false;
        boolean email_ok=false;


        public InputFormViewHolder(View itemView) {
            super(itemView);

            mob_no=(EditText)itemView.findViewById(R.id.mob_no);
            email=(EditText)itemView.findViewById(R.id.emailid);
            go_btn=(Button)itemView.findViewById(R.id.submit_form);

            go_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.e("###","Button clicked from Input Form Adapter");
                    ChatUtil util=new ChatUtil();

                    if (!mob_no.getText().toString().matches("[0-9]{10}")){
                        mob_no.setError(context.getResources().getString(R.string.validnumber));
                        mob_ok=false;
                    }else{
                        mob_ok=true;
                    }
                    if (!email.getText().toString().matches("[a-zA-Z._]+[@][a-z]{3,}[.][a-z]{2,3}")){
                        email.setError(context.getResources().getString(R.string.validemail));
                        email_ok=false;
                    }else{
                        email_ok=true;
                    }
                    if (email_ok && mob_ok){
                        ChatGlobal.text = ChatGlobal.text +"\nContact No. = "+mob_no.getText().toString()+"\nemail id = "+email.getText().toString();
                        util.setReplyMessage(context.getResources().getString(R.string.record));
//                    util.startspeaking("Your query has been recorded. We will get back to you shortly.");
                        util.sendmail(context.getResources().getString(R.string.emailsubject), ChatGlobal.text);
                        ChatGlobal.text="";
                    }



                }
            });
        }

    }
}
