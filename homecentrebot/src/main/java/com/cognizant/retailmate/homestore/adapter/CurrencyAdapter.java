package com.cognizant.retailmate.homestore.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cognizant.retailmate.homestore.R;
import com.cognizant.retailmate.homestore.model.CurrencyModel;

import java.util.ArrayList;

/**
 * Created by 540472 on 3/21/2017.
 */
public class CurrencyAdapter extends RecyclerView.Adapter<CurrencyAdapter.CurrencyViewHolder> {
    Context context;
    ArrayList<CurrencyModel> currencyModelArrayList;

    public CurrencyAdapter(Context context, ArrayList<CurrencyModel> currencyModelArrayList) {
        this.context = context;
        this.currencyModelArrayList = currencyModelArrayList;
    }

    @Override
    public CurrencyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.card_search,parent,false);
        CurrencyViewHolder currencyViewHolder=new CurrencyViewHolder(view);
        return currencyViewHolder;
    }

    @Override
    public void onBindViewHolder(CurrencyViewHolder holder, int position) {
        //CurrencyModel currencyModel=currencyModelArrayList.get(position);
        holder.INR.setText(String.valueOf(currencyModelArrayList.get(0).getCurrencyvalue()));
        holder.USD.setText(String.valueOf(currencyModelArrayList.get(1).getCurrencyvalue()));
        holder.EUR.setText(String.valueOf(currencyModelArrayList.get(2).getCurrencyvalue()));
        holder.JPY.setText(String.valueOf(currencyModelArrayList.get(3).getCurrencyvalue()));
        holder.GBP.setText(String.valueOf(currencyModelArrayList.get(4).getCurrencyvalue()));
        holder.AUD.setText(String.valueOf(currencyModelArrayList.get(5).getCurrencyvalue()));
    }

    @Override
    public int getItemCount() {
        return 1;
    }

    public class CurrencyViewHolder extends RecyclerView.ViewHolder{
        TextView INR,USD,EUR,JPY,GBP,AUD;

        public CurrencyViewHolder(View itemView) {
            super(itemView);
            INR=(TextView)itemView.findViewById(R.id.inr);
            USD=(TextView)itemView.findViewById(R.id.usd);
            EUR=(TextView)itemView.findViewById(R.id.eur);
            JPY=(TextView)itemView.findViewById(R.id.jpy);
            GBP=(TextView)itemView.findViewById(R.id.gbp);
            AUD=(TextView)itemView.findViewById(R.id.aud);
        }
    }
}
