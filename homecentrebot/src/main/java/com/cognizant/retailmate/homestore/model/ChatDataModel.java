package com.cognizant.retailmate.homestore.model;

import java.util.ArrayList;

/**
 * Created by 543898 on 10/18/2016.
 */
public class ChatDataModel {
    String mDataset;
    String mTime;
    Integer mDatasetTypes;
    ArrayList<ProductDataModel> mProductArray;
    ArrayList<SingleInvoice> singleInvoices;
    ArrayList<OfferDataModel> mOfferArray;
    ArrayList<modelsizechart> modelsizechartList;

    public ArrayList<OptionsDataModel> getOptionsList() {
        return OptionsList;
    }

    public void setOptionsList(ArrayList<OptionsDataModel> optionsList) {
        OptionsList = optionsList;
    }

    ArrayList<OptionsDataModel>  OptionsList;

    public ArrayList<modelsizechart> getModelsizechartList() {
        return modelsizechartList;
    }

    public void setModelsizechartList(ArrayList<modelsizechart> modelsizechartList) {
        this.modelsizechartList = modelsizechartList;
    }

    public ArrayList<CurrencyModel> currencyModels;

    public ArrayList<OfferDataModel> getmOfferArray() {
        return mOfferArray;
    }

    public void setmOfferArray(ArrayList<OfferDataModel> mOfferArray) {
        this.mOfferArray = mOfferArray;
    }



    public ArrayList<CurrencyModel> getCurrencyModels() {
        return currencyModels;
    }

    public void setCurrencyModels(ArrayList<CurrencyModel> currencyModels) {
        this.currencyModels = currencyModels;
    }



    public String getmDataset() {
        return mDataset;
    }

    public void setmDataset(String mDataset) {
        this.mDataset = mDataset;
    }

    public String getmTime() {
        return mTime;
    }

    public void setmTime(String mTime) {
        this.mTime = mTime;
    }

    public Integer getmDatasetTypes() {
        return mDatasetTypes;
    }

    public void setmDatasetTypes(Integer mDatasetTypes) {
        this.mDatasetTypes = mDatasetTypes;
    }

    public ArrayList<ProductDataModel> getmProductArray() {
        return mProductArray;
    }

    public void setmProductArray(ArrayList<ProductDataModel> mProductArray) {
        this.mProductArray = mProductArray;
    }

    public ArrayList<SingleInvoice> getSingleInvoices() {
        return singleInvoices;
    }

    public void setSingleInvoices(ArrayList<SingleInvoice> singleInvoices) {
        this.singleInvoices = singleInvoices;
    }
}
