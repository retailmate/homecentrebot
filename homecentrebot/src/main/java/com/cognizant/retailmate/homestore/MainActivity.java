package com.cognizant.retailmate.homestore;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.speech.tts.Voice;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.cognizant.retailmate.homestore.adapter.ChatAdapter;
import com.cognizant.retailmate.homestore.model.ChatDataModel;
import com.cognizant.retailmate.homestore.model.CurrencyModel;
import com.cognizant.retailmate.homestore.model.ModelClass;
import com.cognizant.retailmate.homestore.model.OfferDataModel;
import com.cognizant.retailmate.homestore.model.ProductDataModel;
import com.cognizant.retailmate.homestore.model.modelsizechart;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;
import java.util.UUID;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okio.BufferedSink;

import static com.cognizant.retailmate.homestore.ChatGlobal.isDelivery;
import static com.cognizant.retailmate.homestore.ChatGlobal.sendButton;

//import com.example.a543898.louisintegrated.model.GMailSender;

public class MainActivity extends AppCompatActivity {
    public static String genre;
    Boolean callMade = false;
    Boolean st=false;
    String authorization_key;
    CallAPI runner;


    LocalBroadcastManager mLocalBroadcastManager;
    String finalText;
//    String notext = "Sorry!,I didn't get that.\n Would you like to share your contact details for clarification of your query ?";
//    String notext=getString(R.string.unknowntext);
    static int result = 5432;
    public static Context context;
    public static TextToSpeech textToSpeech;
    public static int chatPosition = -1;
    ImageView mic;
    int check = 111;
    protected boolean isOptionClicked = false;
    UUID session_id = UUID.randomUUID();

    ArrayList<CurrencyModel> currencyModels;
    ArrayList<modelsizechart>  modelsizechartArrayList;
    // Storage Permissions
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    ChatUtil util = new ChatUtil();
    ChatDataModel currencyReqeust = new ChatDataModel();

    BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e("%%%%", intent.getStringExtra("reply"));
            ChatGlobal.genre=intent.getStringExtra("reply");
            if (ChatGlobal.genre.equalsIgnoreCase("Yes")){
                Log.e("##", "genre = yes");
                if (ChatGlobal.isEmail){
                    ChatGlobal.isEmail=false;
                    Log.e("###","----------isEmail----yes pressed");
                    util.showInputForm();
                }else if(ChatGlobal.isDelivery||ChatGlobal.isDeliveryNo){
                    ChatGlobal.isDelivery=false;
                    ChatGlobal.isDeliveryYes=true;
                    util.setReplyMessage("Please call 800-MYHOME and discuss the issue with our dedicated agents, we will help you out");

                }else if(ChatGlobal.isInvoice){
                    ChatGlobal.isInvoiceYes=true;

                    if(ChatGlobal.isInvoiceNo){
                        Toast.makeText(MainActivity.this, "only one option allowed", Toast.LENGTH_SHORT).show();
                    }
                    else  if(ChatGlobal.isInvoiceYes) {
                        ChatGlobal.isInvoiceNo=false;
                        util.showInput();
                    }

                }

            }else if (ChatGlobal.genre.equalsIgnoreCase("No")){
                Log.e("##", "genre = no");
                if (ChatGlobal.isEmail){
                    ChatGlobal.isEmail=false;
                    Log.e("###","----------isEmail ----no pressed");
                    util.setReplyMessage(context.getResources().getString(R.string.thanks));
                    util.sendmail("Instorebot Unresolved Query",ChatGlobal.text);
                    Toast.makeText(MainActivity.this, "Email sent to corresponding manager", Toast.LENGTH_SHORT).show();
                }else if(ChatGlobal.isDelivery||ChatGlobal.isDeliveryYes){
                    ChatGlobal.isDelivery=false;
                    ChatGlobal.isDeliveryNo=true;
                    util.setReplyMessage("Please bring your receipt to the store you purchased from and get the credit note or refund");

                }else if(ChatGlobal.isInvoice){
                    ChatGlobal.isInvoiceNo=true;
                    if(ChatGlobal.isInvoiceYes){
                        Toast.makeText(MainActivity.this, "only one option....", Toast.LENGTH_SHORT).show();
                    }
                    else if(ChatGlobal.isInvoiceNo) {
                        ChatGlobal.isInvoiceYes=false;
                        util.setReplyMessage(context.getResources().getString(R.string.thanks));
                    }

                }

            }
            else {
//                Log.e("genre", ChatGlobal.genre);
                displaychart();
            }
            Log.e("genre", ChatGlobal.genre);
            isOptionClicked = true;

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mLocalBroadcastManager = LocalBroadcastManager.getInstance(this);
        IntentFilter mIntentFilter = new IntentFilter();
        mIntentFilter.addAction("option.click");
        mLocalBroadcastManager.registerReceiver(mMessageReceiver, mIntentFilter);


        String languageToLoad="en";
        Intent i = getIntent();
        if (i!=null) {
            languageToLoad = i.getStringExtra("languageToLoad");
        }
    if(languageToLoad.equals("en")){
        authorization_key="Bearer CYFTPXVOJT5RMRLY33NXMFTDP3PDGH6E";
    }
        else if(languageToLoad.equals("ar")){
        authorization_key="Bearer 5V5GH3YMEQALZVSDQLJDY6WUZG4QK3W2";
//   getSupportActionBar().setTitle("HomeStore Bot (عربى)");
    }

        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
        getSupportActionBar().setTitle(getString(R.string.app_name));
        setContentView(R.layout.activity_main);
        //FOR INITIALIZING mContext in ChatGlobal.java
        ChatGlobal chatGlobal = new ChatGlobal(getApplicationContext());
        context = getApplicationContext();
        sendButton = (ImageButton) findViewById(R.id.enter_chat1);
        ChatGlobal.chatText = (EditText) findViewById(R.id.chat_edit_text);

        ChatGlobal.mDatasetTypes = new ArrayList<Integer>();
        ChatGlobal.mDataset = new ArrayList<String>();

        ChatGlobal.chatDataModels = new ArrayList<>();
        mic = (ImageView) findViewById(R.id.voice);

        ChatGlobal.mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        ChatGlobal.mLayoutManager = new LinearLayoutManager(MainActivity.this);
        ChatGlobal.mRecyclerView.setLayoutManager(ChatGlobal.mLayoutManager);
        ChatGlobal.mAdapter = new ChatAdapter(ChatGlobal.mContext, ChatGlobal.chatDataModels);
        ChatGlobal.mRecyclerView.setAdapter(ChatGlobal.mAdapter);
        ChatGlobal.mRecyclerView.scrollToPosition(ChatGlobal.chatDataModels.size() - 1);

        textToSpeech = new TextToSpeech(MainActivity.this, new TextToSpeech.OnInitListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onInit(int status) {
                if (status == textToSpeech.SUCCESS) {
                    result = textToSpeech.setLanguage(Locale.ENGLISH);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        Set<Voice> voices = textToSpeech.getVoices();
                        Iterator<Voice> iterator = voices.iterator();
                        while (iterator.hasNext()) {
                            Log.e("TAG", "Voice = " + iterator.next());
                        }
                    }
                } else {

                    Toast.makeText(getApplicationContext(), "Not supported in your device", Toast.LENGTH_SHORT).show();
                }
            }
        });


        mic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startlistening();
            }
        });

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChatGlobal.isEmail=false;
                ChatGlobal.isInvoice=false;
                ChatGlobal.isDelivery=false;
                InputMethodManager imm = (InputMethodManager) getSystemService(getApplicationContext().INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(sendButton.getApplicationWindowToken(), 0);

                chatPosition = chatPosition + 1;
                ChatGlobal.text = String.valueOf(ChatGlobal.chatText.getText());
                ChatGlobal.text = ChatGlobal.text.trim();
                if (!ChatGlobal.text.matches("")) {
                    ChatGlobal.chatText.setText("");

                    setSendMessage(ChatGlobal.text);
                    ChatGlobal.mAdapter.notifyDataSetChanged();
                    ChatGlobal.mRecyclerView.scrollToPosition(chatPosition - 1);
                    runner = new CallAPI();
                    runner.execute(ChatGlobal.text);

                }
                textToSpeech.stop();
            }
        });

        ChatGlobal.mRecyclerView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override

            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                ChatGlobal.mRecyclerView.scrollToPosition(ChatGlobal.chatDataModels.size() - 1);
            }
        });

//invoice <code></code>

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == check && resultCode == RESULT_OK) {
            ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            ChatGlobal.chatText.setText(result.get(0));
            sendButton.performClick();

        }
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();

        textToSpeech.shutdown();
       LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
//        super.onDestroy();
    }


    private void showcurrency() {
        verifyStoragePermissions(MainActivity.this);
        ChatUtil.verifyStoragePermissions(MainActivity.this);
        //box model for offers
        ChatDataModel chatOfferData = new ChatDataModel();

        chatOfferData.setmDatasetTypes(ChatGlobal.SHOW_CURRENCY);
        Date d1 = new Date();
        String time1 = (String) DateFormat.format("HH:mm", d1.getTime());
        chatOfferData.setmTime(time1);
        currencyReqeust.setmDatasetTypes(ChatGlobal.SHOW_CURRENCY);
        ChatGlobal.chatDataModels.add(currencyReqeust);
        ChatGlobal.mAdapter.notifyDataSetChanged();
        ChatGlobal.mRecyclerView.scrollToPosition(ChatGlobal.chatDataModels.size() - 1);
    }


    public void loadProductsFromAssets() {
        String jsonreplyString = util.loadJSONFromAsset("product.json");
        Log.e("TAG", "Response = " + jsonreplyString);
        try {
            JSONObject replyObj = new JSONObject(jsonreplyString);
            JSONArray recommendArray = replyObj.getJSONArray("value");

            //box model for offers
            ChatDataModel chatRecommendData = new ChatDataModel();
            ArrayList<ProductDataModel> productDataModelArrayList = new ArrayList<>();
            chatRecommendData.setmDatasetTypes(ChatGlobal.RECEIVE_PRODUCT);
            Date d1 = new Date();
            String time1 = (String) DateFormat.format("HH:mm", d1.getTime());
            chatRecommendData.setmTime(time1);

            util.setReplyMessage(getString(R.string.newproduct));
            for (int i = 0; i < recommendArray.length(); i++) {
                JSONObject productObj = recommendArray.getJSONObject(i);
                ProductDataModel productDataModel = new ProductDataModel();
                productDataModel.setProductName(productObj.getString("ProductName"));
                productDataModel.setProductId(productObj.getString("ProductId"));
                productDataModel.setPrice(productObj.getDouble("UnitPrice"));
                String uri = productObj.getString("Image");
                Log.e("TAG", "image = " + uri);
                productDataModel.setImageSource(util.getResourceId(uri, "drawable", getPackageName()));
                Log.e("TAG", "productDataModel image = " + productDataModel.getImageSource());

                productDataModelArrayList.add(productDataModel);
            }
            util.setReplyProductOffline(productDataModelArrayList, chatRecommendData);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public void loadChart()
    {
        ChatGlobal.optionsDataModel = new ArrayList<>();
        util.setSuggestDataModels();
        util.setOption(ChatGlobal.optionsDataModel);
    }
    public void loaddeliveryoptions(){
        ChatGlobal.optionsDataModel = new ArrayList<>();
        util.setSuggestBooleanModels();
        util.setOption(ChatGlobal.optionsDataModel);

    }

    public void displaychart()
    {
        st=true;
//       isOptionClicked=false;
        ChatDataModel sizechartdata = new ChatDataModel();
        modelsizechartArrayList= new ArrayList<>();
        modelsizechart onechart=new modelsizechart();
//

            if (ChatGlobal.genre.equals("Gents")) {
                onechart.setPic(R.drawable.gtw);

            } else if (ChatGlobal.genre.equals("Ladies")) {
                onechart.setPic(R.drawable.ltw);
//                st=true;


            } else if (ChatGlobal.genre.equals("Kids")) {
                onechart.setPic(R.drawable.kidsw);

//                st=true;
            }

        modelsizechartArrayList.add(onechart);
        sizechartdata.setmDatasetTypes(ChatGlobal.SHOW_LCHART);
        util.setsizechart(modelsizechartArrayList,sizechartdata);
againoptions();
    }
    public void againoptions() {
    ChatGlobal.genre="na";
        if (st) {
            loadChart();
        }
    }


    public void loadTextureFromSdCard()
    {
        //Check for storage permission
        verifyStoragePermissions(MainActivity.this);

        //box model for offers
        ChatDataModel chatOfferData = new ChatDataModel();
        ArrayList<OfferDataModel> offerDataModelArrayList = new ArrayList<>();
        chatOfferData.setmDatasetTypes(ChatGlobal.RECEIVE_OFFER);
        Date d1 = new Date();
        String time1 = (String) DateFormat.format("HH:mm", d1.getTime());
        chatOfferData.setmTime(time1);

        String path = Environment.getExternalStorageDirectory() + "/LandMark/test.xls";
        File file = new File(path);
        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            Workbook wb = Workbook.getWorkbook(fileInputStream);
            Sheet sheet = wb.getSheet(0);
            int rows = sheet.getRows();
            for (int i = 0; i < rows; i++) {
                OfferDataModel offerDataModel = new OfferDataModel();
                for (int j = 0; j < 3; j++) {
                    Cell c = sheet.getCell(j, i);

                    if (j == 0) {
                        offerDataModel.setOfferTitle(c.getContents());
                    } else if (j == 1) {
                        offerDataModel.setOfferDescription(c.getContents());
                    } else {
                        offerDataModel.setValidity(c.getContents());
                    }

                    Log.e("###", "at cell (" + j + "," + i + ") = " + c.getContents());
                }
                offerDataModelArrayList.add(offerDataModel);
            }
            util.setReplyOffer(offerDataModelArrayList, chatOfferData);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (BiffException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void verifyStoragePermissions(Activity activity)
    {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }


    private void setCurrencyCard()
    {

        System.out.println("@@## currency size" + currencyReqeust.currencyModels.size());

        ChatGlobal.chatDataModels.add(currencyReqeust);
        ChatGlobal.mAdapter.notifyDataSetChanged();
        ChatGlobal.mRecyclerView.scrollToPosition(ChatGlobal.chatDataModels.size() - 1);
    }

    private void startlistening()
    {
        Intent i = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        i.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        i.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        i.putExtra(RecognizerIntent.EXTRA_PROMPT, "You should be speaking!!");
        try {
            startActivityForResult(i, check);
        } catch (ActivityNotFoundException a) {
            a.printStackTrace();
            Toast.makeText(getApplicationContext(), "Problem!!!", Toast.LENGTH_SHORT).show();
        }

    }

    public void setReceiveMessage(String s)
    {
        ChatDataModel chatData = new ChatDataModel();
        String text = s.trim();
        if (!text.matches("")) {
            chatData.setmDataset(text);
            chatData.setmDatasetTypes(ChatGlobal.RECEIVE);
            Date d = new Date();
            String time = (String) DateFormat.format("HH:mm", d.getTime());
            chatData.setmTime(time);
            ChatGlobal.chatDataModels.add(chatData);

            ChatGlobal.mAdapter.notifyDataSetChanged();
            ChatGlobal.mRecyclerView.scrollToPosition(ChatGlobal.chatDataModels.size() - 1);
        }
    }


    public void setSendMessage(String s)
    {
        ChatDataModel chatData = new ChatDataModel();
        String text = s.trim();
        if (!text.matches("")) {
            chatData.setmDataset(text);
            Log.e("send",text);
            chatData.setmDatasetTypes(ChatGlobal.SEND);
            Date d = new Date();
            String time = (String) DateFormat.format("HH:mm", d.getTime());
            chatData.setmTime(time);
            ChatGlobal.chatDataModels.add(chatData);

            ChatGlobal.mAdapter.notifyDataSetChanged();
            ChatGlobal.mRecyclerView.scrollToPosition(ChatGlobal.chatDataModels.size() - 1);
        }
    }

    public class CallAPI extends AsyncTask<String, String, String>
    {

        public CallAPI() {
            //set context variables if required
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }


        @Override
        protected String doInBackground(String... params) {
            System.out.println("Running........................");

            String textMessage = params[0]; // URL to call
            String resultToDisplay = "";

            OkHttpClient client = new OkHttpClient();

            Request request = new Request.Builder()
                    .url(" https://api.wit.ai/converse?v=20170320&session_id="+session_id+"&q=" + textMessage)
                    .post(new RequestBody() {
                        @Override
                        public MediaType contentType() {
                            return null;
                        }

                        @Override
                        public void writeTo(BufferedSink bufferedSink) throws IOException {

                        }
                    })

                    .addHeader("authorization", authorization_key)
                    .addHeader("content-type", "application/json")
                    .addHeader("accept", "application/json")
                    .build();


            try {
                Response response = client.newCall(request).execute();

                resultToDisplay = response.body().string();

                return resultToDisplay;
            } catch (IOException e) {
                e.printStackTrace();
            }
            return resultToDisplay;
        }

        @Override
        protected void onPostExecute(String result) {
            //Update the UI
            Boolean flag = true;

            Log.e("##$$", result);
            if (result != null && result.length() > 0) {
                try {
                    Gson gson = new Gson();
                    ModelClass modelClass = gson.fromJson(result, ModelClass.class);
                    result = modelClass.getType();
                    if(modelClass.getEntities().getIntent().isEmpty())
                    {
                        Log.e("###","is empty");
                        flag = true;
                    }
                          if ((result.equals("action")) || (result.equals("stop"))) {
                                if (result.equals("stop"))
                                {
                                        callMade = true;
                                        flag =false;
                                        makeapicall();
                                 }

                                 if (modelClass.getAction().equals("loadOffers")||modelClass.getAction().equals("جيتوفي"))
                                 {
                                    //offers code call
                                    loadTextureFromSdCard();

                                 }
                                 else if (modelClass.getAction().equals("getInvoiceDetail")||modelClass.getAction().equals("جيتب"))
                                 {
                                           //invoice code call
                                           util.showInput();
                                     ChatGlobal.isInvoice=true;
                                     ChatGlobal.isInvoiceYes=false;
                                     ChatGlobal.isInvoiceNo=false;
                                 }
                                 else if (modelClass.getAction().equals("getCurrencyExchange")||modelClass.getAction().equals("جيتكورنسي"))
                                 {
                                            //Exchange currency code call
                                               showcurrency();
                                 }
                                 else if (modelClass.getAction().equals("getProductDetails")||modelClass.getAction().equals("جيتبرودوكت"))
                                 {
                                         //productdetails code call
                                           loadProductsFromAssets();
                                  }
                                 else
                                 {
                                          makeapicall();
                                 }
                        }
                          else
                          {
                            if (modelClass.getMsg().equals("please select category from below options")||modelClass.getMsg().equals("الرجاء تحديد الفئة من أ"))
                            {
//                                Log.e("ct",""+modelClass.getMsg());
                                setReceiveMessage(modelClass.getMsg());
                                startspeaking(modelClass.getMsg());
                                         flag= false;
                                         loadChart();

                            }else if (modelClass.getMsg().equals("Is the product delivered?")){
                                ChatGlobal.isDelivery=true;
                                setReceiveMessage(modelClass.getMsg());
                                loaddeliveryoptions();

                            }
                            else {
                                finalText = modelClass.getMsg().replace("\\n", System.getProperty("line.separator"));
//                                Log.e("cc",""+finalText);
                                setReceiveMessage(finalText);
                                startspeaking(finalText);
                            }
                        }

                }
                catch (Exception e) {
                      if (flag){
                        e.printStackTrace();
                        Log.e("TAG", "if contition false here2");
//                          util.sendmail("Instorebot Unresolved Query",text);
//                          Toast.makeText(MainActivity.this, "Email sent to corresponding manager", Toast.LENGTH_SHORT).show();
                          setReceiveMessage(getString(R.string.unknowntext));
                          startspeaking(getString(R.string.unknowntext));
                          ChatGlobal.isEmail=true;
                          util.loadOptionChart();
                }

                }


            }
        }
    }






    private void makeapicall()
    {
        runner.cancel(true);
        runner = new CallAPI();
        runner.execute(ChatGlobal.text);
    }

    public static void startspeaking(String to_tell)
    {
        if (result == textToSpeech.LANG_NOT_SUPPORTED || result == textToSpeech.LANG_MISSING_DATA)
            Toast.makeText(MainActivity.context, "Not supported in your device", Toast.LENGTH_SHORT).show();
        else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                ttsGreater21(to_tell);
            } else {
                ttsUnder20(to_tell);
            }
        }
    }

    @SuppressWarnings("deprecation")
    public static void ttsUnder20(String text)
    {
        HashMap<String, String> map = new HashMap<>();
        map.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "MessageId");
        textToSpeech.speak(text, TextToSpeech.QUEUE_ADD, map);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static void ttsGreater21(String text)
    {
        String utteranceId = MainActivity.context.hashCode() + "";
        textToSpeech.speak(text, TextToSpeech.QUEUE_ADD, null, utteranceId);
    }


}

